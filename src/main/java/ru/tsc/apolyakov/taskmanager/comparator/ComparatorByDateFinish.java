package ru.tsc.apolyakov.taskmanager.comparator;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.api.entity.IHasDateFinish;

import java.util.Comparator;

public final class ComparatorByDateFinish implements Comparator<IHasDateFinish> {

    @NotNull
    private static final ComparatorByDateFinish INSTANCE = new ComparatorByDateFinish();

    private ComparatorByDateFinish() {
    }

    @NotNull
    public static ComparatorByDateFinish getInstance() {
        return INSTANCE;
    }

    @Override
    public final int compare(@Nullable final IHasDateFinish o1, @Nullable final IHasDateFinish o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getDateFinish() == null && o2.getDateFinish() == null) return 0;
        if (o1.getDateFinish() == null) return -1;
        if (o2.getDateFinish() == null) return 1;
        return o1.getDateFinish().compareTo(o2.getDateFinish());
    }

}
