package ru.tsc.apolyakov.taskmanager.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.api.service.IPropertyService;
import ru.tsc.apolyakov.taskmanager.bootstrap.Bootstrap;

public class Backup extends Thread {

    @NotNull
    private static final String LOAD_BACKUP_COMMAND = "load backup";

    @NotNull
    private static final String SAVE_BACKUP_COMMAND = "save backup";

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final IPropertyService propertyService;

    public Backup(@NotNull final Bootstrap bootstrap, @NotNull final IPropertyService propertyService) {
        this.bootstrap = bootstrap;
        this.propertyService = propertyService;
        this.setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    private void load() {
        bootstrap.executeCommand(LOAD_BACKUP_COMMAND);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            Thread.sleep(propertyService.getAutosaveFrequency());
            save();
        }
    }

    private void save() {
        bootstrap.executeCommand(SAVE_BACKUP_COMMAND);
    }

}
