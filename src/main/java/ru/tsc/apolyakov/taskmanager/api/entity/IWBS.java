package ru.tsc.apolyakov.taskmanager.api.entity;

public interface IWBS extends IHasCreated, IHasDateFinish, IHasDateStart, IHasName, IHasStatus {

}
