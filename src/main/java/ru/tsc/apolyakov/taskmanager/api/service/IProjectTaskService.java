package ru.tsc.apolyakov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.model.Project;
import ru.tsc.apolyakov.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IProjectTaskService {

    @Nullable
    Task addTaskToProject(@NotNull String projectId, @NotNull String taskId);

    void clearProjects();

    @NotNull
    List<Task> findAllTasksByProjectId(@NotNull String projectId, @NotNull Comparator<Task> taskComparator);

    @Nullable
    Project removeProjectById(@Nullable String projectId);

    @Nullable
    Project removeProjectByIndex(int projectIndex);

    @Nullable
    Project removeProjectByName(@NotNull String name);

    @Nullable
    Task removeTaskFromProject(@NotNull String projectId, @NotNull String taskId);

}
