package ru.tsc.apolyakov.taskmanager.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.model.AbstractModel;

import java.util.List;

public interface IRepository<E extends AbstractModel> {

    void add(@NotNull E entity);

    void addAll(@Nullable List<E> entities);

    void clear();

    @NotNull
    List<E> findAll();

    @Nullable
    E findById(@NotNull String id);

    int getSize();

    boolean isEmpty();

    void remove(@NotNull E entity);

    @Nullable
    E removeById(@NotNull String id);

}
