package ru.tsc.apolyakov.taskmanager.api.repository;

import org.jetbrains.annotations.Nullable;

public interface IAuthRepository {

    @Nullable String getCurrentUserId();

    void setCurrentUserId(@Nullable String currentUserId);

}
