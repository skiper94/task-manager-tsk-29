package ru.tsc.apolyakov.taskmanager.api.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.enumerated.Status;

public interface IHasStatus {

    @NotNull Status getStatus();

    void setStatus(@NotNull Status status);

}
