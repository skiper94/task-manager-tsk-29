package ru.tsc.apolyakov.taskmanager.api.service;

import ru.tsc.apolyakov.taskmanager.api.property.IApplicationProperty;
import ru.tsc.apolyakov.taskmanager.api.property.IAutosaveProperty;
import ru.tsc.apolyakov.taskmanager.api.property.IPasswordProperty;

public interface IPropertyService extends IPasswordProperty, IApplicationProperty, IAutosaveProperty {

}
