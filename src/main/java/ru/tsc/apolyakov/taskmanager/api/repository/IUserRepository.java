package ru.tsc.apolyakov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.api.IRepository;
import ru.tsc.apolyakov.taskmanager.enumerated.Role;
import ru.tsc.apolyakov.taskmanager.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable User findByEmail(@NotNull String email);

    @Nullable User findByLogin(@NotNull String login);

    @Nullable String findIdByLogin(@NotNull String login);

    boolean isFoundByEmail(@NotNull String email);

    boolean isFoundByLogin(@NotNull String login);

    void removeByLogin(@NotNull String login);

    @Nullable User update(@NotNull String id, @NotNull String login, @NotNull String password, @NotNull String email,
                          @NotNull Role role, @Nullable String firstName, @Nullable String middleName, @Nullable String lastName);

    void clear();
}
