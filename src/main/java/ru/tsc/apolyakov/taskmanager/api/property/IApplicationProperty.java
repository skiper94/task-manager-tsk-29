package ru.tsc.apolyakov.taskmanager.api.property;

import org.jetbrains.annotations.NotNull;

public interface IApplicationProperty {

    @NotNull String getApplicationVersion();

}
