package ru.tsc.apolyakov.taskmanager.api.repository;

import ru.tsc.apolyakov.taskmanager.api.IBusinessEntityRepository;
import ru.tsc.apolyakov.taskmanager.model.Project;

public interface IProjectRepository extends IBusinessEntityRepository<Project> {

}
