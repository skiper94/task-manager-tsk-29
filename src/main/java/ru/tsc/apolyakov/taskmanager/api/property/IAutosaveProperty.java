package ru.tsc.apolyakov.taskmanager.api.property;

import org.jetbrains.annotations.NotNull;

public interface IAutosaveProperty {

    @NotNull Integer getAutosaveFrequency();

    @NotNull Boolean isAutosaveOnExit();

}
