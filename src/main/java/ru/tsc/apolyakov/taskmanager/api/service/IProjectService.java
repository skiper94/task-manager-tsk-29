package ru.tsc.apolyakov.taskmanager.api.service;

import ru.tsc.apolyakov.taskmanager.api.IBusinessEntityService;
import ru.tsc.apolyakov.taskmanager.model.Project;

public interface IProjectService extends IBusinessEntityService<Project> {

}
