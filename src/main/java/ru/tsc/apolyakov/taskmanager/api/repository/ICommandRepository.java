package ru.tsc.apolyakov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.command.AbstractCommand;

import java.util.Map;

public interface ICommandRepository {

    @NotNull Map<String, AbstractCommand> getArguments();

    @NotNull Map<String, AbstractCommand> getCommands();

}
