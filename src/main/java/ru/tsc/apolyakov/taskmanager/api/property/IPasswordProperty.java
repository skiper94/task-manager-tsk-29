package ru.tsc.apolyakov.taskmanager.api.property;

import org.jetbrains.annotations.NotNull;

public interface IPasswordProperty {

    @NotNull Integer getPasswordIteration();

    @NotNull String getPasswordSecret();

}
