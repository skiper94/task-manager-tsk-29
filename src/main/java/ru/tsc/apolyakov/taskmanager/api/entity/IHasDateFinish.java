package ru.tsc.apolyakov.taskmanager.api.entity;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasDateFinish {

    @Nullable Date getDateFinish();

    void setDateFinish(@Nullable Date date);

}
