package ru.tsc.apolyakov.taskmanager.enumerated;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.comparator.*;

import java.util.Comparator;

public enum Sort {

    CREATED("CREATED - Sort by created date", ComparatorByCreated.getInstance()),
    STARTED("STARTED - Sort by start date", ComparatorByDateStart.getInstance()),
    FINISHED("FINISHED - Sort by finish date", ComparatorByDateFinish.getInstance()),
    NAME("NAME - Sort by name", ComparatorByName.getInstance()),
    STATUS("STATUS - Sort by status", ComparatorByStatus.getInstance());

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator comparator;

    Sort(@NotNull String displayName, @NotNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

    @Override
    public String toString() {
        return displayName;
    }

}
