package ru.tsc.apolyakov.taskmanager.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Status {

    PLANNED("Planned"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }

}