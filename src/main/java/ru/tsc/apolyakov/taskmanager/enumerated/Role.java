package ru.tsc.apolyakov.taskmanager.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {

    USER("User"),
    ADMIN("Admin");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }

}
