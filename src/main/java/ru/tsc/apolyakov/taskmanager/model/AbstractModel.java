package ru.tsc.apolyakov.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.util.NumberUtil;

import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractModel implements Serializable {

    @NotNull
    private String id = NumberUtil.generateId();

}
