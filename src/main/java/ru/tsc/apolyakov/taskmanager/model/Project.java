package ru.tsc.apolyakov.taskmanager.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.api.entity.IWBS;

@NoArgsConstructor
public final class Project extends AbstractBusinessEntity implements IWBS {

    public Project(@NotNull final String name, @NotNull final String userId) {
        super(name, userId);
    }

    public Project(@NotNull final String name, @Nullable final String description, @NotNull final String userId) {
        super(name, description, userId);
    }

}
