package ru.tsc.apolyakov.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.api.entity.IWBS;
import ru.tsc.apolyakov.taskmanager.enumerated.Status;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static ru.tsc.apolyakov.taskmanager.constant.StringConst.*;
import static ru.tsc.apolyakov.taskmanager.util.ValidationUtil.isEmptyString;

@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractBusinessEntity extends AbstractModel implements IWBS {

    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateFinish;

    @Nullable
    private Date dateStart;

    @Nullable
    private String description;

    @NotNull
    private String name;

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    private String userId;

    public AbstractBusinessEntity(@NotNull final String name, @NotNull final String userId) {
        this.name = name;
        this.userId = userId;
    }

    public AbstractBusinessEntity(@NotNull final String name, @Nullable final String description, @NotNull final String userId) {
        this.name = name;
        this.description = description != null ? description : EMPTY;
        this.userId = userId;
    }

    @Override
    public String toString() {
        @NotNull final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return getId()
                + DELIMITER
                + (!isEmptyString(name) ? name : PLACEHOLDER)
                + DELIMITER
                + (!isEmptyString(description) ? description : PLACEHOLDER)
                + DELIMITER
                + formatter.format(created)
                + DELIMITER
                + status
                + DELIMITER
                + (dateStart == null ? PLACEHOLDER : formatter.format(dateStart))
                + DELIMITER
                + (dateFinish == null ? PLACEHOLDER : formatter.format(dateFinish))
                + DELIMITER
                + getUserId();
    }

}
