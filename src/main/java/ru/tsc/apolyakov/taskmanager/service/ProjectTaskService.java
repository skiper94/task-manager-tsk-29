package ru.tsc.apolyakov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.api.service.IAuthService;
import ru.tsc.apolyakov.taskmanager.api.service.IProjectService;
import ru.tsc.apolyakov.taskmanager.api.service.IProjectTaskService;
import ru.tsc.apolyakov.taskmanager.api.service.ITaskService;
import ru.tsc.apolyakov.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.apolyakov.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.apolyakov.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.apolyakov.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.apolyakov.taskmanager.model.Project;
import ru.tsc.apolyakov.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static ru.tsc.apolyakov.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.apolyakov.taskmanager.util.ValidationUtil.isInvalidListIndex;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    public ProjectTaskService(@NotNull final ITaskService taskService, @NotNull final IProjectService projectService, @NotNull final IAuthService authService) {
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Nullable
    @Override
    public final Task addTaskToProject(@NotNull final String projectId, @NotNull final String taskId) {
        if (isEmptyString(projectId) || isEmptyString(taskId)) throw new IdEmptyException();
        if (projectService.isNotFoundById(projectId)) throw new ProjectNotFoundException();
        return (taskService.addTaskToProject(taskId, projectId));
    }

    @Override
    public final void clearProjects() {
        projectService.findAll().forEach(project -> taskService.removeAllByProjectId(project.getId()));
        projectService.clear();
    }

    @NotNull
    @Override
    public final List<Task> findAllTasksByProjectId(@NotNull final String projectId, @NotNull final Comparator<Task> taskComparator) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        if (projectService.isNotFoundById(projectId)) throw new ProjectNotFoundException();
        return taskService.findAllByProjectId(projectId, taskComparator);
    }

    @Nullable
    @Override
    public final Project removeProjectById(@Nullable final String projectId) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        Optional.ofNullable(projectService.findById(projectId)).orElseThrow(ProjectNotFoundException::new);
        taskService.removeAllByProjectId(projectId);
        return projectService.removeById(projectId);
    }

    @Nullable
    @Override
    public final Project removeProjectByIndex(final int index) {
        if (isInvalidListIndex(index, projectService.getSize())) throw new IndexIncorrectException(index + 1);
        final String id = projectService.getId(index);
        return removeProjectById(id);
    }

    @Nullable
    @Override
    public final Project removeProjectByName(@NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        return removeProjectById(Optional.ofNullable(projectService.getId(name)).orElseThrow(ProjectNotFoundException::new));
    }

    @Nullable
    @Override
    public final Task removeTaskFromProject(@NotNull final String projectId, @NotNull final String taskId) {
        if (isEmptyString(projectId) || isEmptyString(taskId)) throw new IdEmptyException();
        if (projectService.isNotFoundById(projectId)) throw new ProjectNotFoundException();
        return (taskService.removeTaskFromProject(taskId, projectId));
    }

}
