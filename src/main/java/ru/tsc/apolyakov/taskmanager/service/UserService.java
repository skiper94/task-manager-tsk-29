package ru.tsc.apolyakov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.api.repository.IUserRepository;
import ru.tsc.apolyakov.taskmanager.api.service.IPropertyService;
import ru.tsc.apolyakov.taskmanager.api.service.IUserService;
import ru.tsc.apolyakov.taskmanager.enumerated.Role;
import ru.tsc.apolyakov.taskmanager.exception.empty.EmailEmptyException;
import ru.tsc.apolyakov.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.apolyakov.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.apolyakov.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.apolyakov.taskmanager.exception.entity.UserExistsWIthEmailException;
import ru.tsc.apolyakov.taskmanager.exception.entity.UserExistsWithLoginException;
import ru.tsc.apolyakov.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.apolyakov.taskmanager.model.User;

import java.util.Objects;
import java.util.Optional;

import static ru.tsc.apolyakov.taskmanager.util.HashUtil.salt;
import static ru.tsc.apolyakov.taskmanager.util.ValidationUtil.isEmptyString;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IUserRepository userRepository, @NotNull final IPropertyService propertyService) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @Override
    public final void add(@NotNull final String login, @NotNull final String password, @NotNull final String email, @NotNull final Role role,
                          @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        if (isEmptyString(email)) throw new EmailEmptyException();
        if (userRepository.isFoundByLogin(login)) throw new UserExistsWithLoginException(login);
        if (userRepository.isFoundByEmail(email)) throw new UserExistsWIthEmailException(email);
        userRepository.add(new User(login, Objects.requireNonNull(salt(password, propertyService)), email, firstName, middleName, lastName, role));
    }

    @Nullable
    @Override
    public final User findByLogin(@NotNull final String login) {
        @NotNull final String id = Optional.ofNullable(findIdByLogin(login)).orElseThrow(UserNotFoundException::new);
        return findById(id);
    }

    @Nullable
    private String findIdByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        return userRepository.findIdByLogin(login);
    }

    @Override
    public final boolean lockById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final User user = Optional.ofNullable(userRepository.findById(id)).orElseThrow(UserNotFoundException::new);
        if (user.isLocked()) return false;
        user.setLocked(true);
        return true;
    }

    @Override
    public final boolean lockByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new IdEmptyException();
        @NotNull final User user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        if (user.isLocked()) return false;
        user.setLocked(true);
        return true;
    }

    @Nullable
    @Override
    public final User removeByLogin(@NotNull final String login) {
        @NotNull final String id = Optional.ofNullable(findIdByLogin(login)).orElseThrow(UserNotFoundException::new);
        return removeById(id);
    }

    @Override
    public final void setPassword(@NotNull final String login, @NotNull final String password) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        @NotNull final User user = Optional.ofNullable(findByLogin(login)).orElseThrow(UserNotFoundException::new);
        user.setPasswordHash(Objects.requireNonNull(salt(password, propertyService)));
    }

    @Override
    public final void setRole(@NotNull final String login, @NotNull final Role role) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final User user = Optional.ofNullable(findByLogin(login)).orElseThrow(UserNotFoundException::new);
        user.setRole(role);
    }

    @Override
    public final boolean unlockById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final User user = Optional.ofNullable(userRepository.findById(id)).orElseThrow(UserNotFoundException::new);
        if (!user.isLocked()) return false;
        user.setLocked(false);
        return true;
    }

    @Override
    public final boolean unlockByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final User user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        if (!user.isLocked()) return false;
        user.setLocked(false);
        return true;
    }

    @Nullable
    @Override
    public final User updateById(@NotNull final String id, @NotNull final String login, @NotNull final String password, @NotNull final String email,
                                 @NotNull final Role role, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        if (isEmptyString(email)) throw new EmailEmptyException();
        @Nullable final User userFoundWithThisLogin = userRepository.findByLogin(login);
        @Nullable final User userFoundWithThisEmail = userRepository.findByEmail(email);
        if (userFoundWithThisLogin != null && !id.equals(userFoundWithThisLogin.getId()))
            throw new UserExistsWithLoginException(login);
        if (userFoundWithThisEmail != null && !id.equals(userFoundWithThisEmail.getId()))
            throw new UserExistsWIthEmailException(email);
        return userRepository.update(id, login, password, email, role, firstName, middleName, lastName);
    }

    @Nullable
    @Override
    public final User updateByLogin(@NotNull final String login, @NotNull final String password, @NotNull final String email,
                                    @NotNull final Role role, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        @NotNull final String id = Optional.ofNullable(userRepository.findIdByLogin(login)).orElseThrow(UserNotFoundException::new);
        return updateById(id, login, password, email, role, firstName, middleName, lastName);
    }

}
