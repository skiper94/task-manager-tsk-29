package ru.tsc.apolyakov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.api.IBusinessEntityRepository;
import ru.tsc.apolyakov.taskmanager.api.IBusinessEntityService;
import ru.tsc.apolyakov.taskmanager.api.service.IAuthService;
import ru.tsc.apolyakov.taskmanager.enumerated.Status;
import ru.tsc.apolyakov.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.apolyakov.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.apolyakov.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.apolyakov.taskmanager.exception.security.AccessDeniedNotAuthorizedException;
import ru.tsc.apolyakov.taskmanager.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static ru.tsc.apolyakov.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.apolyakov.taskmanager.util.ValidationUtil.isInvalidListIndex;

public abstract class AbstractBusinessEntityService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessEntityService<E> {

    @NotNull
    protected final IAuthService authService;

    @NotNull
    protected final IBusinessEntityRepository<E> repository;

    public AbstractBusinessEntityService(@NotNull final IBusinessEntityRepository<E> repository, @NotNull final IAuthService authService) {
        super(repository);
        this.repository = repository;
        this.authService = authService;
    }

    @Override
    public void add(@NotNull final String name, @Nullable final String description) {
        if (isEmptyString(name)) throw new NameEmptyException();
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        repository.add(authService.getCurrentUserId(), name, description);
    }

    @Override
    public void clear() {
        if (authService.isPrivilegedUser() || isEmptyString(authService.getCurrentUserId())) repository.clear();
        else repository.clearForUser(authService.getCurrentUserId());
    }

    @Nullable
    @Override
    public E completeById(@NotNull final String id) {
        return updateStatusById(id, Status.COMPLETED);
    }

    @Nullable
    @Override
    public E completeByIndex(final int index) {
        return updateStatusByIndex(index, Status.COMPLETED);
    }

    @Nullable
    @Override
    public E completeByName(@NotNull final String name) {
        return updateStatusByName(name, Status.COMPLETED);
    }

    @NotNull
    @Override
    public List<E> findAll() {
        if (authService.isPrivilegedUser() || (isEmptyString(authService.getCurrentUserId())))
            return repository.findAll();
        return repository.findAllForUser(authService.getCurrentUserId());
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.findAll(comparator);
        return repository.findAllForUser(authService.getCurrentUserId(), comparator);
    }

    @Nullable
    @Override
    public E findById(@Nullable final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.findById(id);
        return repository.findByIdForUser(authService.getCurrentUserId(), id);
    }

    @Nullable
    @Override
    public E findByIndex(final int index) {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.findByIndex(index);
        return repository.findByIndexForUser(authService.getCurrentUserId(), index);
    }

    @Nullable
    @Override
    public E findByName(@NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.findByName(name);
        return repository.findByNameForUser(authService.getCurrentUserId(), name);
    }

    @Nullable
    @Override
    public String getId(final int index) {
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.getId(index);
        return repository.getIdForUser(authService.getCurrentUserId(), index);
    }

    @Nullable
    @Override
    public String getId(@NotNull final String name) {
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.getId(name);
        return repository.getIdForUser(authService.getCurrentUserId(), name);
    }

    @Override
    public int getSize() {
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.getSize();
        else return repository.getSizeForUser(authService.getCurrentUserId());
    }

    @Override
    public boolean isEmpty() {
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.isEmpty();
        return repository.isEmptyForUser(authService.getCurrentUserId());
    }

    @Override
    public boolean isNotFoundById(@NotNull final String id) {
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.isNotFoundById(id);
        return repository.isNotFoundByIdForUser(authService.getCurrentUserId(), id);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.removeById(id);
        return repository.removeByIdForUser(authService.getCurrentUserId(), id);
    }

    @Nullable
    @Override
    public E removeByIndex(final int index) {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.removeByIndex(index);
        return repository.removeByIndexForUser(authService.getCurrentUserId(), index);
    }

    @Nullable
    @Override
    public E removeByName(@NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.removeByName(name);
        return repository.removeByNameForUser(authService.getCurrentUserId(), name);
    }

    @Nullable
    @Override
    public E startById(@NotNull final String id) {
        return updateStatusById(id, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    public E startByIndex(final int index) {
        return updateStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    public E startByName(@NotNull final String name) {
        return updateStatusByName(name, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    public E updateById(@NotNull final String id, @NotNull final String name, @Nullable final String description) {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (isEmptyString(name)) throw new NameEmptyException();
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.update(id, name, description);
        return repository.updateForUser(authService.getCurrentUserId(), id, name, description);
    }

    @Nullable
    @Override
    public E updateByIndex(final int index, @NotNull final String name, @Nullable final String description) {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        @Nullable final String id = Optional.ofNullable(getId(index)).orElse(null);
        if (id == null) return null;
        return updateById(id, name, description);
    }

    @Nullable
    @Override
    public E updateStatusById(@NotNull final String id, @NotNull final Status status) {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.updateStatusById(id, status);
        return repository.updateStatusByIdForUser(authService.getCurrentUserId(), id, status);
    }

    @Nullable
    @Override
    public E updateStatusByIndex(final int index, @NotNull final Status status) {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.updateStatusByIndex(index, status);
        return repository.updateStatusByIndexForUser(authService.getCurrentUserId(), index, status);
    }

    @Nullable
    @Override
    public E updateStatusByName(@NotNull final String name, @NotNull final Status status) {
        if (isEmptyString(name)) throw new NameEmptyException();
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.updateStatusByName(name, status);
        return repository.updateStatusByNameForUser(authService.getCurrentUserId(), name, status);
    }

}
