package ru.tsc.apolyakov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.api.repository.ITaskRepository;
import ru.tsc.apolyakov.taskmanager.api.service.IAuthService;
import ru.tsc.apolyakov.taskmanager.api.service.ITaskService;
import ru.tsc.apolyakov.taskmanager.exception.security.AccessDeniedNotAuthorizedException;
import ru.tsc.apolyakov.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.apolyakov.taskmanager.util.ValidationUtil.isEmptyString;

public final class TaskService extends AbstractBusinessEntityService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository repository;

    public TaskService(@NotNull final ITaskRepository repository, @NotNull final IAuthService authService) {
        super(repository, authService);
        this.repository = repository;
    }

    @Nullable
    @Override
    public final Task addTaskToProject(@NotNull final String taskId, @NotNull final String projectId) {
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.addTaskToProject(taskId, projectId);
        return repository.addTaskToProjectForUser(authService.getCurrentUserId(), taskId, projectId);
    }

    @NotNull
    @Override
    public final List<Task> findAllByProjectId(@NotNull final String projectId, @NotNull final Comparator<Task> comparator) {
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.findAllByProjectId(projectId, comparator);
        return repository.findAllByProjectIdForUser(authService.getCurrentUserId(), projectId, comparator);
    }

    @Override
    public final void removeAllByProjectId(@NotNull final String projectId) {
        repository.removeAllByProjectId(projectId);
    }

    @Nullable
    @Override
    public final Task removeTaskFromProject(@NotNull final String taskId, @NotNull final String projectId) {
        if (isEmptyString(authService.getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.removeTaskFromProject(taskId, projectId);
        return repository.removeTaskFromProjectForUser(authService.getCurrentUserId(), taskId, projectId);
    }

}
