package ru.tsc.apolyakov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.api.repository.IProjectRepository;
import ru.tsc.apolyakov.taskmanager.api.service.IAuthService;
import ru.tsc.apolyakov.taskmanager.api.service.IProjectService;
import ru.tsc.apolyakov.taskmanager.model.Project;

public final class ProjectService extends AbstractBusinessEntityService<Project> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository repository, @NotNull final IAuthService authService) {
        super(repository, authService);
    }

}
