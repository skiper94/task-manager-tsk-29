package ru.tsc.apolyakov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.api.repository.ICommandRepository;
import ru.tsc.apolyakov.taskmanager.api.service.ICommandService;
import ru.tsc.apolyakov.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NotNull
    @Override
    public final Map<String, AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @NotNull
    @Override
    public final List<AbstractCommand> getCommandList() {
        return new ArrayList<>(commandRepository.getCommands().values());
    }

    @NotNull
    @Override
    public final Map<String, AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

}
