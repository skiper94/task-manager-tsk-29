package ru.tsc.apolyakov.taskmanager.exception.security;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Access denied!";

    public AccessDeniedException() {
        super(MESSAGE);
    }

}
