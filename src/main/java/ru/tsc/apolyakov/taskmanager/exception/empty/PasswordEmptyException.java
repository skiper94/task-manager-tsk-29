package ru.tsc.apolyakov.taskmanager.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.exception.AbstractException;

public final class PasswordEmptyException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Password is empty.";

    public PasswordEmptyException() {
        super(MESSAGE);
    }

}
