package ru.tsc.apolyakov.taskmanager.exception.incorrect;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.exception.AbstractException;

public final class IndexIncorrectException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Incorrect index: ";

    public IndexIncorrectException(final int index) {
        super(MESSAGE + index);
    }

}
