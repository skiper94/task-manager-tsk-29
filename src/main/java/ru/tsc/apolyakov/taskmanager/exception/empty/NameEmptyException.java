package ru.tsc.apolyakov.taskmanager.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.exception.AbstractException;

public final class NameEmptyException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Name is empty.";

    public NameEmptyException() {
        super(MESSAGE);
    }

}
