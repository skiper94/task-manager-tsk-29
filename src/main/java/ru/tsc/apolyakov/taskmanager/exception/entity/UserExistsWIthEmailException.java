package ru.tsc.apolyakov.taskmanager.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.exception.AbstractException;

public final class UserExistsWIthEmailException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "User already exists with email: ";

    public UserExistsWIthEmailException(final String email) {
        super(MESSAGE + email);
    }

}
