package ru.tsc.apolyakov.taskmanager.exception.security;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.exception.AbstractException;

public final class UserSelfLockNotAllowedException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Cannot lock logged-on user.";

    public UserSelfLockNotAllowedException() {
        super(MESSAGE);
    }

}
