package ru.tsc.apolyakov.taskmanager.exception.incorrect;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.exception.AbstractException;

public final class IncorrectCredentialsException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Incorrect credentials.";

    public IncorrectCredentialsException() {
        super(MESSAGE);
    }

}
