package ru.tsc.apolyakov.taskmanager.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.exception.AbstractException;

public final class CommandNotFoundException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Unknown command or argument: ";

    public CommandNotFoundException(final String command) {
        super(MESSAGE + command);
    }

}
