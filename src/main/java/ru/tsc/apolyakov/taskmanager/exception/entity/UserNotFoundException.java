package ru.tsc.apolyakov.taskmanager.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.exception.AbstractException;

public final class UserNotFoundException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "User not found.";

    public UserNotFoundException() {
        super(MESSAGE);
    }

}
