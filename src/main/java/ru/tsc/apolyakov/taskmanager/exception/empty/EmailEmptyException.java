package ru.tsc.apolyakov.taskmanager.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.exception.AbstractException;

public final class EmailEmptyException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Email is empty.";

    public EmailEmptyException() {
        super(MESSAGE);
    }

}
