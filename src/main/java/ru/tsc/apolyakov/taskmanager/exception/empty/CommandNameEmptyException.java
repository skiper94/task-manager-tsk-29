package ru.tsc.apolyakov.taskmanager.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.exception.AbstractException;

public final class CommandNameEmptyException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Command name cannot be empty.";

    public CommandNameEmptyException() {
        super(MESSAGE);
    }
}
