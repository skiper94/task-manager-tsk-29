package ru.tsc.apolyakov.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readNumber;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "start project by index";

    @NotNull
    private final static String DESCRIPTION = "start project by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int index = readNumber(INDEX_INPUT);
        throwExceptionIfNull(getProjectService().startByIndex(index - 1));
    }

}
