package ru.tsc.apolyakov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.enumerated.Role;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readLine;

public final class UserUnlockByIdCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "unlock user by id";

    @NotNull
    private static final String DESCRIPTION = "unlock user by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        if (!getUserService().unlockById(id)) printLinesWithEmptyLine(USER_IS_NOT_LOCKED);
    }

}
