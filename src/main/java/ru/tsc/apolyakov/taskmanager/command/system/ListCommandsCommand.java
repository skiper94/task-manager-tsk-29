package ru.tsc.apolyakov.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.command.AbstractCommand;

import static ru.tsc.apolyakov.taskmanager.util.ValidationUtil.isEmptyString;

public final class ListCommandsCommand extends AbstractCommand {

    @NotNull
    private final static String CMD_NAME = "list arguments";

    @NotNull
    private final static String DESCRIPTION = "list available command-line arguments";

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public final String getArgument() {
        return null;
    }

    @Override
    public final void execute() {
        System.out.println();
        for (@NotNull final AbstractCommand command : getCommandService().getCommandList()) {
            if (isEmptyString(command.getArgument())) continue;
            System.out.println(command.getArgument());
        }
        System.out.println();
    }

}
