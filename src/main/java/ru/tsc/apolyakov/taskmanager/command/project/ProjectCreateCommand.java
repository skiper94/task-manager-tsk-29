package ru.tsc.apolyakov.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readLine;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "create project";

    @NotNull
    private final static String DESCRIPTION = "create a new project";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        @NotNull final String description = readLine(DESCRIPTION_INPUT);
        getProjectService().add(name, description);
        printLinesWithEmptyLine("Done. Type <list projects> to view all projects.");
    }

}
