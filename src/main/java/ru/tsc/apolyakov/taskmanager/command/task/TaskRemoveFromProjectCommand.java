package ru.tsc.apolyakov.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readLine;

public final class TaskRemoveFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "remove task from project";

    @NotNull
    private final static String DESCRIPTION = "remove task from a project";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String projectId = readLine(PROJECT_ID_INPUT);
        @NotNull final String taskId = readLine(TASK_ID_INPUT);
        throwExceptionIfNull(getProjectTaskService().removeTaskFromProject(projectId, taskId));
    }

}
