package ru.tsc.apolyakov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.enumerated.Role;
import ru.tsc.apolyakov.taskmanager.exception.security.UserSelfLockNotAllowedException;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readLine;

public final class UserLockByIdCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "lock user by id";

    @NotNull
    private static final String DESCRIPTION = "lock user by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        if (id.equals(getAuthService().getCurrentUserId())) throw new UserSelfLockNotAllowedException();
        if (!getUserService().lockById(id)) printLinesWithEmptyLine(USER_ALREADY_LOCKED);
    }

}
