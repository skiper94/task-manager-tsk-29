package ru.tsc.apolyakov.taskmanager.command.domain;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.dto.Domain;
import ru.tsc.apolyakov.taskmanager.enumerated.Role;

import java.io.FileOutputStream;

public class DomainSaveXMLFasterXMLCommand extends AbstractDomainCommand {

    @NotNull
    private final static String NAME = "save xml fasterxml";

    @NotNull
    private final static String DESCRIPTION = "save projects, tasks and users to xml file using fasterxml";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final Domain domain = getDomain();
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final String xml = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
