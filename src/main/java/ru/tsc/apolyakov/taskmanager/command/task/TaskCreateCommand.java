package ru.tsc.apolyakov.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readLine;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "create task";

    @NotNull
    private final static String DESCRIPTION = "create a new task";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        @NotNull final String description = readLine(DESCRIPTION_INPUT);
        getTaskService().add(name, description);
        printLinesWithEmptyLine("Done. Type <list tasks> to view all tasks.");
    }

}
