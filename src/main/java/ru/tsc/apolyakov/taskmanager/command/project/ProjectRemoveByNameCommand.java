package ru.tsc.apolyakov.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readLine;

public final class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "remove project by name";

    @NotNull
    private final static String DESCRIPTION = "remove project by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        throwExceptionIfNull(getProjectTaskService().removeProjectByName(name));
    }

}
