package ru.tsc.apolyakov.taskmanager.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.command.AbstractCommand;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public class VersionCommand extends AbstractCommand {

    @NotNull
    private final static String CMD_NAME = "version";

    @NotNull
    private final static String ARG_NAME = "-v";

    @NotNull
    private final static String DESCRIPTION = "show version info";

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final String getArgument() {
        return ARG_NAME;
    }

    @Override
    public final void execute() {
        printLinesWithEmptyLine(Manifests.read("build"));
    }

}
