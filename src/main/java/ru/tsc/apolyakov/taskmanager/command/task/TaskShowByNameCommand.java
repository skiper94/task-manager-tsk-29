package ru.tsc.apolyakov.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.model.Task;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readLine;

public final class TaskShowByNameCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "show task by name";

    @NotNull
    private final static String DESCRIPTION = "show task by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        @Nullable final Task task = getTaskService().findByName(name);
        showTask(task);
    }

}
