package ru.tsc.apolyakov.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.model.Project;

import java.util.Comparator;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.*;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "list projects";

    @NotNull
    private final static String DESCRIPTION = "show all projects";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        if (getProjectService().isEmpty()) {
            printLinesWithEmptyLine("No projects yet. Type <create project> to add a project.");
            return;
        }
        @NotNull final Comparator<Project> comparator = readComparator();
        printListWithIndexes(getProjectService().findAll(comparator));
    }

}
