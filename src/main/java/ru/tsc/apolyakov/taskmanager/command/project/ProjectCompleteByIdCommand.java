package ru.tsc.apolyakov.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readLine;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "complete project by id";

    @NotNull
    private final static String DESCRIPTION = "complete project by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        throwExceptionIfNull(getProjectService().completeById(id));
    }

}
