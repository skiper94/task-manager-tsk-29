package ru.tsc.apolyakov.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.command.AbstractCommand;

public final class ListArgumentsCommand extends AbstractCommand {

    @NotNull
    private final static String CMD_NAME = "list commands";

    @NotNull
    private final static String DESCRIPTION = "list available commands";

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public final String getArgument() {
        return null;
    }

    @Override
    public final void execute() {
        System.out.println();
        for (@NotNull final AbstractCommand command : getCommandService().getCommandList()) {
            System.out.println(command.getCommand());
        }
        System.out.println();
    }

}
