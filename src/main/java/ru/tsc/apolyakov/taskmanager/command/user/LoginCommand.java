package ru.tsc.apolyakov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readLine;

public final class LoginCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "login";

    @NotNull
    private static final String DESCRIPTION = "login into the system";

    {
        setNeedAuthorization(false);
    }

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String login = readLine(ENTER_LOGIN);
        @NotNull final String password = readLine(ENTER_PASSWORD);
        getAuthService().login(login, password);
    }

}
