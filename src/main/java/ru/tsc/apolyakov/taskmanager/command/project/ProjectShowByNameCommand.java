package ru.tsc.apolyakov.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.model.Project;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readLine;

public final class ProjectShowByNameCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "show project by name";

    @NotNull
    private final static String DESCRIPTION = "show project by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        @Nullable final Project project = getProjectService().findByName(name);
        showProject(project);
    }

}
