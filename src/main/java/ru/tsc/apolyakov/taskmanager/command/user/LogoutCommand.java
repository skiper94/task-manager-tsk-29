package ru.tsc.apolyakov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;

public final class LogoutCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "logout";

    @NotNull
    private static final String DESCRIPTION = "logout from the system";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        getAuthService().logout();
    }
}
