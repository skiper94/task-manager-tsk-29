package ru.tsc.apolyakov.taskmanager.command.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.dto.Domain;
import ru.tsc.apolyakov.taskmanager.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DomainLoadJsonFasterXMLCommand extends AbstractDomainCommand {

    @NotNull
    private final static String NAME = "load json fasterxml";

    @NotNull
    private final static String DESCRIPTION = "load projects, tasks and users from json file using fasterxml";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
        logoutAfterLoad();
    }

}
