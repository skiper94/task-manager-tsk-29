package ru.tsc.apolyakov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.enumerated.Role;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readLine;
import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readRole;

public final class UserUpdateByLoginCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "update user by login";

    @NotNull
    private static final String DESCRIPTION = "update user by login";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String login = readLine(ENTER_LOGIN);
        @NotNull final String password = readLine(ENTER_PASSWORD);
        @NotNull final String email = readLine(ENTER_EMAIL);
        @NotNull final Role role = readRole(ENTER_ROLE);
        @NotNull final String firstName = readLine(ENTER_FIRST_NAME);
        @NotNull final String middleName = readLine(ENTER_MIDDLE_NAME);
        @NotNull final String lastName = readLine(ENTER_LAST_NAME);
        throwExceptionIfNull(getUserService().updateByLogin(login, password, email, role, firstName, middleName, lastName));
    }

}
