package ru.tsc.apolyakov.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.model.Project;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readNumber;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "show project by index";

    @NotNull
    private final static String DESCRIPTION = "show project by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int index = readNumber(INDEX_INPUT);
        @Nullable final Project project = getProjectService().findByIndex(index - 1);
        showProject(project);
    }

}
