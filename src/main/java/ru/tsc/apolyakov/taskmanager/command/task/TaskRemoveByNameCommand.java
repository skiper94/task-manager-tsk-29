package ru.tsc.apolyakov.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readLine;

public final class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "remove task by name";

    @NotNull
    private final static String DESCRIPTION = "remove task by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        throwExceptionIfNull(getTaskService().removeByName(name));
    }

}
