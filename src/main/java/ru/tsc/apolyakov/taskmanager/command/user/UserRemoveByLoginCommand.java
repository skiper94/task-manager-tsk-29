package ru.tsc.apolyakov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.enumerated.Role;
import ru.tsc.apolyakov.taskmanager.exception.security.UserSelfDeleteNotAllowedException;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readLine;

public final class UserRemoveByLoginCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "remove user by login";

    @NotNull
    private static final String DESCRIPTION = "remove user by login";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String login = readLine(ENTER_LOGIN);
        if (login.equals(getAuthService().getCurrentUserLogin())) throw new UserSelfDeleteNotAllowedException();
        throwExceptionIfNull(getUserService().removeByLogin(login));
    }

}
