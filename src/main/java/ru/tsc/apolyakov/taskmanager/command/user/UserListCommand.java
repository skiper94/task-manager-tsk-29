package ru.tsc.apolyakov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.enumerated.Role;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.printList;

public final class UserListCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "list users";

    @NotNull
    private static final String DESCRIPTION = "show all users";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        if (getUserService().isEmpty()) {
            printLinesWithEmptyLine("No users found!");
            return;
        }
        printList(getUserService().findAll());
    }

}
