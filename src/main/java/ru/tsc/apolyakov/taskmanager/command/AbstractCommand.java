package ru.tsc.apolyakov.taskmanager.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.api.ServiceLocator;
import ru.tsc.apolyakov.taskmanager.api.service.IAuthService;
import ru.tsc.apolyakov.taskmanager.api.service.ICommandService;
import ru.tsc.apolyakov.taskmanager.api.service.IPropertyService;
import ru.tsc.apolyakov.taskmanager.enumerated.Role;
import ru.tsc.apolyakov.taskmanager.exception.other.ServiceLocatorNotFoundException;

import java.util.Optional;

import static ru.tsc.apolyakov.taskmanager.util.ValidationUtil.isEmptyString;

public abstract class AbstractCommand {

    @NotNull
    protected static final String ID_INPUT = "Please enter id: ";

    @NotNull
    protected static final String INDEX_INPUT = "Please enter index: ";

    @NotNull
    protected static final String NAME_INPUT = "Please enter name: ";

    @NotNull
    protected static final String DESCRIPTION_INPUT = "Please enter description: ";

    @Nullable
    protected ServiceLocator serviceLocator;

    protected boolean needAuthorization = false;

    public abstract void execute();

    @Nullable
    public abstract String getArgument();

    @NotNull
    protected IAuthService getAuthService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getAuthService();
    }

    @NotNull
    protected ICommandService getCommandService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getCommandService();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getPropertyService();
    }

    @NotNull
    public abstract String getCommand();

    @NotNull
    public abstract String getDescription();

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        return (getCommand() + (isEmptyString(getArgument()) ? "" : " [" + getArgument() + "]") + " - " + getDescription() + ".");
    }

    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

    public boolean getNeedAuthorization() {
        return needAuthorization;
    }

    public void setNeedAuthorization(final boolean needAuthorization) {
        this.needAuthorization = needAuthorization;
    }
}
