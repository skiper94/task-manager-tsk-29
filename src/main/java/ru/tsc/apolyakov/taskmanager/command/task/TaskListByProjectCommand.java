package ru.tsc.apolyakov.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.*;

public final class TaskListByProjectCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "list tasks by project";

    @NotNull
    private final static String DESCRIPTION = "show all tasks in a project";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String projectId = readLine(PROJECT_ID_INPUT);
        @NotNull final Comparator<Task> taskComparator = readComparator();
        @NotNull final List<Task> taskList = getProjectTaskService().findAllTasksByProjectId(projectId, taskComparator);
        if (taskList.isEmpty()) {
            printLinesWithEmptyLine("No tasks yet. Type <add task to project> to add a task to a project.");
            return;
        }
        printListWithIndexes(taskList);
    }

}
