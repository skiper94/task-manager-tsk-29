package ru.tsc.apolyakov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.exception.security.AccessDeniedNotAuthorizedException;
import ru.tsc.apolyakov.taskmanager.model.User;

import java.util.Optional;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readLine;

public final class ChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "change password";

    @NotNull
    private static final String DESCRIPTION = "change user's password";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String currentUserId = Optional.ofNullable(getAuthService().getCurrentUserId())
                .orElseThrow(AccessDeniedNotAuthorizedException::new);
        @Nullable final User user = getUserService().findById(currentUserId);
        Optional.ofNullable(user).orElseThrow(AccessDeniedNotAuthorizedException::new);
        @Nullable final String login;
        if (getAuthService().isPrivilegedUser()) login = readLine(ENTER_LOGIN);
        else login = user.getLogin();
        @NotNull final String password = readLine(ENTER_PASSWORD);
        getUserService().setPassword(login, password);
    }

}
