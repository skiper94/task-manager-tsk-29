package ru.tsc.apolyakov.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readLine;
import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readNumber;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "update project by index";

    @NotNull
    private final static String DESCRIPTION = "update project by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int index = readNumber(INDEX_INPUT);
        @NotNull final String name = readLine(NAME_INPUT);
        @NotNull final String description = readLine(DESCRIPTION_INPUT);
        throwExceptionIfNull(getProjectService().updateByIndex(index - 1, name, description));
    }

}
