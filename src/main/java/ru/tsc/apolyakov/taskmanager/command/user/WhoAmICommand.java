package ru.tsc.apolyakov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.exception.security.AccessDeniedNotAuthorizedException;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.apolyakov.taskmanager.util.ValidationUtil.isEmptyString;

public final class WhoAmICommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "whoami";

    @NotNull
    private static final String DESCRIPTION = "print current user login";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        if (isEmptyString(getAuthService().getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        printLinesWithEmptyLine(getUserService().findById(getAuthService().getCurrentUserId()));
    }

}
