package ru.tsc.apolyakov.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.command.AbstractCommand;
import ru.tsc.apolyakov.taskmanager.util.NumberUtil;

import static ru.tsc.apolyakov.taskmanager.constant.StringConst.*;
import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public final class InfoCommand extends AbstractCommand {

    @NotNull
    private final static String CMD_NAME = "info";

    @NotNull
    private final static String ARG_NAME = "-i";

    @NotNull
    private final static String DESCRIPTION = "show system info";

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final String getArgument() {
        return ARG_NAME;
    }

    @Override
    public final void execute() {
        @NotNull final Runtime runtime = Runtime.getRuntime();
        final int numberOfCpus = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final long maxMemory = runtime.maxMemory();
        @Nullable final String maxMemoryDisplayed;
        if (maxMemory == Long.MAX_VALUE) maxMemoryDisplayed = SYSINFO_NO_LIMIT_TEXT;
        else maxMemoryDisplayed = NumberUtil.convertBytesToString(maxMemory);
        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        printLinesWithEmptyLine(SYSINFO_PROCESSORS + numberOfCpus,
                SYSINFO_FREE_MEMORY + NumberUtil.convertBytesToString(freeMemory),
                SYSINFO_MAX_MEMORY + maxMemoryDisplayed,
                SYSINFO_TOTAL_MEMORY + NumberUtil.convertBytesToString(totalMemory),
                SYSINFO_USED_MEMORY + NumberUtil.convertBytesToString(usedMemory));
    }

}
