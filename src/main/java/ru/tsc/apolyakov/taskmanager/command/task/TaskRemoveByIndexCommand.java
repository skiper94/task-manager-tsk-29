package ru.tsc.apolyakov.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readNumber;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "remove task by index";

    @NotNull
    private final static String DESCRIPTION = "remove task by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final int index = readNumber(INDEX_INPUT);
        throwExceptionIfNull(getTaskService().removeByIndex(index - 1));
    }

}
