package ru.tsc.apolyakov.taskmanager.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.dto.Domain;
import ru.tsc.apolyakov.taskmanager.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DomainLoadXMLJaxbCommand extends AbstractDomainCommand {

    @NotNull
    private final static String NAME = "load xml jaxb";

    @NotNull
    private final static String DESCRIPTION = "load projects, tasks and users from xml file using jaxb";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final File file = new File(FILE_JAXB_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
        logoutAfterLoad();
    }

}
