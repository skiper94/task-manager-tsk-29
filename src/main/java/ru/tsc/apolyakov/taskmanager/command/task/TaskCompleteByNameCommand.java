package ru.tsc.apolyakov.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.readLine;

public final class TaskCompleteByNameCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "complete task by name";

    @NotNull
    private final static String DESCRIPTION = "complete task by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        throwExceptionIfNull(getTaskService().completeByName(name));
    }

}
