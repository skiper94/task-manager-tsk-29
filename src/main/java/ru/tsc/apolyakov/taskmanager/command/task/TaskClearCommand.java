package ru.tsc.apolyakov.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.apolyakov.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "clear tasks";

    @NotNull
    private final static String DESCRIPTION = "delete all tasks";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int count = getTaskService().getSize();
        getTaskService().clear();
        printLinesWithEmptyLine(count + " tasks removed.");
    }

}
