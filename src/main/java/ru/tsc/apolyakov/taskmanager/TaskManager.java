package ru.tsc.apolyakov.taskmanager;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apolyakov.taskmanager.bootstrap.Bootstrap;

public final class TaskManager {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
