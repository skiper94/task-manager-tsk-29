package ru.tsc.apolyakov.taskmanager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@UtilityClass
public final class NumberUtil {

    @NotNull
    public static String convertBytesToString(final long bytes) {
        @NotNull final String[] units = {"B", "KB", "MB", "GB", "TB"};
        for (int i = 0; i < 5; i++) {
            if (bytes < Math.pow(1024, i + 1))
                return (bytes / (long) Math.pow(1024, i)) + " " + units[i];
        }
        return bytes + " " + units[0];
    }

    @NotNull
    public static String generateId() {
        return UUID.randomUUID().toString();
    }

}
