package ru.tsc.apolyakov.taskmanager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static ru.tsc.apolyakov.taskmanager.util.ValidationUtil.isEmptyString;

@UtilityClass
public final class SystemUtil {

    public long getPID() {
        @Nullable final String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (isEmptyString(processName)) return 0;
        try {
            return Long.parseLong(processName.split("@")[0]);
        } catch (@NotNull final Exception e) {
            return 0;
        }
    }

}
