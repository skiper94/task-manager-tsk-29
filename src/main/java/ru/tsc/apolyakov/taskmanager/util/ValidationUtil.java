package ru.tsc.apolyakov.taskmanager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;

@UtilityClass
public final class ValidationUtil {

    public static boolean isEmptyString(@Nullable final String string) {
        return string == null || string.isEmpty();
    }

    public static boolean isInvalidListIndex(final int index, final int size) {
        return (index < 0 || index >= size);
    }

}
