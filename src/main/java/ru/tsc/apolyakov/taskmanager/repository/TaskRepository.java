package ru.tsc.apolyakov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.api.repository.ITaskRepository;
import ru.tsc.apolyakov.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractBusinessEntityRepository<Task> implements ITaskRepository {

    @Override
    public final void add(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        @NotNull final Task task = new Task(name, description, userId);
        map.put(task.getId(), task);
    }

    @Nullable
    @Override
    public final Task addTaskToProject(@NotNull final String taskId, @NotNull final String projectId) {
        Optional<Task> task = Optional.ofNullable(findById(taskId));
        task.ifPresent(t -> t.setProjectId(projectId));
        return task.orElse(null);
    }

    @Nullable
    @Override
    public final Task addTaskToProjectForUser(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @NotNull Optional<Task> task = Optional.ofNullable(findByIdForUser(userId, taskId));
        task.ifPresent(t -> t.setProjectId(projectId));
        return task.orElse(null);
    }

    @NotNull
    @Override
    public final List<Task> findAllByProjectId(@NotNull final String projectId) {
        return map.values().stream()
                .filter(predicateByProjectId(projectId))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public final List<Task> findAllByProjectId(@NotNull final String projectId, @NotNull final Comparator<Task> comparator) {
        return map.values().stream()
                .filter(predicateByProjectId(projectId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public final List<Task> findAllByProjectIdForUser(@NotNull final String userId, @NotNull final String projectId) {
        return map.values().stream()
                .filter(predicateByUser(userId))
                .filter(predicateByProjectId(projectId))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public final List<Task> findAllByProjectIdForUser(@NotNull final String userId, @NotNull final String projectId, @NotNull final Comparator<Task> comparator) {
        return map.values().stream()
                .filter(predicateByUser(userId))
                .filter(predicateByProjectId(projectId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public final boolean isNotFoundTaskInProject(@NotNull final String taskId, @NotNull final String projectId) {
        return (!findAllByProjectId(projectId).contains(findById(taskId)));
    }

    @NotNull
    public final Predicate<Task> predicateByProjectId(@NotNull final String projectId) {
        return s -> projectId.equals(s.getProjectId());
    }

    @Override
    public final void removeAllByProjectId(@NotNull final String projectId) {
        findAllByProjectId(projectId).forEach(task -> map.remove(task.getId()));
    }

    @Nullable
    @Override
    public final Task removeTaskFromProject(@NotNull final String taskId, @NotNull final String projectId) {
        @NotNull final Optional<Task> task = Optional.ofNullable(findById(taskId)).filter(predicateByProjectId(projectId));
        task.ifPresent(t -> t.setProjectId(null));
        return task.orElse(null);
    }

    @Nullable
    @Override
    public final Task removeTaskFromProjectForUser(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @NotNull final Optional<Task> task = Optional.ofNullable(findByIdForUser(userId, taskId)).filter(predicateByProjectId(projectId));
        task.ifPresent(t -> t.setProjectId(null));
        return task.orElse(null);
    }

}
