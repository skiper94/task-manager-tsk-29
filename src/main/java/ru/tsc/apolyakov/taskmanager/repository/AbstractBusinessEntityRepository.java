package ru.tsc.apolyakov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.api.IBusinessEntityRepository;
import ru.tsc.apolyakov.taskmanager.enumerated.Status;
import ru.tsc.apolyakov.taskmanager.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractBusinessEntityRepository<E extends AbstractBusinessEntity> extends AbstractModelRepository<E> implements IBusinessEntityRepository<E> {

    @Override
    public void clearForUser(@NotNull final String userId) {
        findKeysForUser(userId).forEach(map::remove);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        return map.values().stream().sorted(comparator).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> findAllForUser(@NotNull final String userId) {
        return map.values().stream()
                .filter(predicateByUser(userId))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> findAllForUser(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return map.values().stream()
                .filter(predicateByUser(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public E findByIdForUser(@NotNull final String userId, @NotNull final String id) {
        final Optional<E> entity = Optional.ofNullable(findById(id));
        return entity.filter(predicateByUser(userId)).orElse(null);
    }

    @Nullable
    @Override
    public E findByIndex(final int index) {
        return map.values().stream()
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E findByIndexForUser(@NotNull final String userId, final int index) {
        return map.values().stream()
                .filter(predicateByUser(userId))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E findByName(@NotNull final String name) {
        return map.values().stream()
                .filter(predicateByName(name))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E findByNameForUser(@NotNull final String userId, @NotNull final String name) {
        return map.values().stream()
                .filter(predicateByUser(userId))
                .filter(predicateByName(name))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public List<String> findKeysForUser(@NotNull final String userId) {
        return map.values().stream()
                .filter(predicateByUser(userId))
                .map(E::getId)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public String getId(@NotNull final String name) {
        @Nullable final E entity = findByName(name);
        return Optional.ofNullable(entity).isPresent() ? entity.getId() : null;
    }

    @Nullable
    @Override
    public String getId(final int index) {
        @Nullable final E entity = findByIndex(index);
        return Optional.ofNullable(entity).isPresent() ? entity.getId() : null;
    }

    @Nullable
    @Override
    public String getIdForUser(@NotNull final String userId, final int index) {
        @Nullable final E entity = findByIndexForUser(userId, index);
        return Optional.ofNullable(entity).isPresent() ? entity.getId() : null;
    }

    @Nullable
    @Override
    public String getIdForUser(@NotNull final String userId, @NotNull final String name) {
        @Nullable final E entity = findByNameForUser(userId, name);
        return Optional.ofNullable(entity).isPresent() ? entity.getId() : null;
    }

    @Override
    public int getSizeForUser(@NotNull final String userId) {
        return findAllForUser(userId).size();
    }

    @Override
    public boolean isEmptyForUser(@NotNull final String userId) {
        return findAllForUser(userId).isEmpty();
    }

    @Override
    public boolean isNotFoundById(@NotNull final String id) {
        return findById(id) == null;
    }

    @Override
    public boolean isNotFoundByIdForUser(@NotNull final String userId, @NotNull final String id) {
        return findByIdForUser(userId, id) == null;
    }

    @NotNull
    public Predicate<AbstractBusinessEntity> predicateByName(@NotNull final String name) {
        return s -> name.equals(s.getName());
    }

    @NotNull
    public Predicate<AbstractBusinessEntity> predicateByUser(@NotNull final String userId) {
        return s -> userId.equals(s.getUserId());
    }

    @Nullable
    @Override
    public E removeByIdForUser(@NotNull final String userId, @NotNull final String id) {
        @Nullable final E entity = findByIdForUser(userId, id);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Nullable
    @Override
    public E removeByIndex(final int index) {
        @Nullable final E entity = findByIndex(index);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Nullable
    @Override
    public E removeByIndexForUser(@NotNull final String userId, final int index) {
        @Nullable final E entity = findByIndexForUser(userId, index);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Nullable
    @Override
    public E removeByName(@NotNull final String name) {
        @Nullable final E entity = findByName(name);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Nullable
    @Override
    public E removeByNameForUser(@NotNull final String userId, @NotNull final String name) {
        @Nullable final E entity = findByNameForUser(userId, name);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Nullable
    @Override
    public E update(@NotNull final String id, @NotNull final String name, @Nullable final String description) {
        final E entity = findById(id);
        return updateEntity(entity, name, description).orElse(null);
    }

    @NotNull
    private Optional<E> updateEntity(@Nullable final E entity, @NotNull final String name, @Nullable final String description) {
        @NotNull final Optional<E> optional = Optional.ofNullable(entity);
        optional.ifPresent(e -> {
            e.setName(name);
            e.setDescription(description);
        });
        return optional;
    }

    @Nullable
    @Override
    public E updateForUser(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @Nullable final String description) {
        @Nullable final E entity = (findByIdForUser(userId, id));
        return updateEntity(entity, name, description).orElse(null);
    }

    @NotNull
    private Optional<E> updateStatus(@Nullable final E entity, @NotNull final Status status) {
        @NotNull final Optional<E> optional = Optional.ofNullable(entity);
        optional.ifPresent(e -> {
            e.setStatus(status);
            if (status.equals(Status.IN_PROGRESS)) e.setDateStart(new Date());
            else if (status.equals(Status.COMPLETED)) e.setDateFinish(new Date());
        });
        return optional;
    }

    @Nullable
    @Override
    public E updateStatusById(@NotNull final String id, @NotNull final Status status) {
        @Nullable final E entity = findById(id);
        return updateStatus(entity, status).orElse(null);
    }

    @Nullable
    @Override
    public E updateStatusByIdForUser(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        @Nullable final E entity = findByIdForUser(userId, id);
        return updateStatus(entity, status).orElse(null);
    }

    @Nullable
    @Override
    public E updateStatusByIndex(final int index, @NotNull final Status status) {
        @NotNull final Optional<String> entityId = Optional.ofNullable(getId(index));
        return entityId.map(id -> updateStatusById(id, status)).orElse(null);
    }

    @Nullable
    @Override
    public E updateStatusByIndexForUser(@NotNull final String userId, final int index, @NotNull final Status status) {
        final Optional<String> entityId = Optional.ofNullable(getIdForUser(userId, index));
        return entityId.map(id -> updateStatusByIdForUser(userId, id, status)).orElse(null);
    }

    @Nullable
    @Override
    public E updateStatusByName(@NotNull final String name, @NotNull final Status status) {
        @NotNull final Optional<String> entityId = Optional.ofNullable(getId(name));
        return entityId.map(id -> updateStatusById(id, status)).orElse(null);
    }

    @Nullable
    @Override
    public E updateStatusByNameForUser(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        final Optional<String> entityId = Optional.ofNullable(getIdForUser(userId, name));
        return entityId.map(id -> updateStatusByIdForUser(userId, id, status)).orElse(null);
    }

}
