package ru.tsc.apolyakov.taskmanager.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.api.repository.IAuthRepository;

public final class AuthRepository implements IAuthRepository {

    @Nullable
    private String currentUserId;

    @Nullable
    @Override
    public String getCurrentUserId() {
        return currentUserId;
    }

    @Override
    public final void setCurrentUserId(@Nullable final String currentUserId) {
        this.currentUserId = currentUserId;
    }
}
