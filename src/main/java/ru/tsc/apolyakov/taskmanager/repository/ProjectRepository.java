package ru.tsc.apolyakov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apolyakov.taskmanager.api.repository.IProjectRepository;
import ru.tsc.apolyakov.taskmanager.model.Project;

public final class ProjectRepository extends AbstractBusinessEntityRepository<Project> implements IProjectRepository {

    @Override
    public final void add(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        @NotNull final Project project = new Project(name, description, userId);
        map.put(project.getId(), project);
    }

}
